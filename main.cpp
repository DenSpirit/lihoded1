#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include"matrix.h"
long clockdiff(timespec t1, timespec t2) {
    return (1000000000 * (t2.tv_sec - t1.tv_sec) + t2.tv_nsec - t1.tv_nsec) / 1000000;
}
void test_blocksize(matrix& a, const matrix& b, int bs) {
    timespec t1, t2;
    fprintf(stderr, "block %d\n", bs);
    clock_gettime(CLOCK_MONOTONIC, &t1);
    a.multiply_block(b, bs);
    clock_gettime(CLOCK_MONOTONIC, &t2);
    printf("%ld\n", clockdiff(t1, t2));
}
int main(int argc, char* argv[]) {
    int n1, n2, n3;
    srand(time(NULL));
    sscanf(argv[1], "%d", &n1);
    //sscanf(argv[2], "%d", &n2);
    //sscanf(argv[3], "%d", &n3);
    //matrix a(n1, n3), b(n3, n2);
    matrix a(n1, n1), b(n1, n1);
    a.randomize();
    b.randomize();
    timespec t1, t2;

    if(argc<=2) {
        fprintf(stderr, "notblock\n");
        clock_gettime(CLOCK_MONOTONIC, &t1);
        a.multiply_single(b);
        clock_gettime(CLOCK_MONOTONIC, &t2);
        printf("%ld\n", clockdiff(t1, t2));
    }

    int bsizes[] = {5, 10, 20, 32, 64};
    if(argc>2) {
        int bs;
        sscanf(argv[2],"%d",&bs);
        test_blocksize(a,b,bs);
    } else {
        for(int i = 0; i < sizeof(bsizes)/sizeof(bsizes[0]); i++) {
            test_blocksize(a,b,bsizes[i]);
        }
    }
    return 0;
}
