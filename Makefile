main: main.cpp matrix.h
	g++ -g -O3 -std=c++14 main.cpp -o main
clean:
	rm main -f
astyle: main.cpp matrix.h
	astyle -p -n main.cpp matrix.h
