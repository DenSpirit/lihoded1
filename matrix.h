#include<stdlib.h>
#include<assert.h>
#include<memory>
using namespace std;
class matrix {
private:
    double** data;
    int h, w;
    void int_multiply_block(const matrix& a,const matrix&b,int bs, int i,int j,int k) {
        int ioff = i*bs;
        int joff = j*bs;
        int koff = k*bs;
        for (int q = 0; q < bs; q++)
            for (int r = 0; r < bs; r++)
                for (int s = 0; s < bs; s++)
                    data[koff + q][joff + r] +=
                    a.data[q + ioff][s + koff] * b.data[s + koff][r + joff];
    }
public:
    matrix(int height, int width): h(height), w(width) {
        data = new double*[h];
        for(int i = 0; i < h; i++) {
            data[i] = new double[w];
        }
    }
    ~matrix() {
        for(int i = 0; i < h; i++) {
            delete[] data[i];
        }
        delete[] data;
    }
    void randomize() {
        for(int i = 0; i < h; i++) {
            for(int j = 0; j < w; j++) {
                int val = rand() % 201 - 100;
                data[i][j] = val;
            }
        }
    }
    unique_ptr<matrix> multiply_single(const matrix& m) {
        assert(w == m.h);
        unique_ptr<matrix> result(new matrix(h, m.w));
        for(int i = 0; i < h; i++) {
            for(int j = 0; j < m.w; j++) {
                for(int k = 0; k < h; k++) {
                    result->data[i][j] += data[i][k] * m.data[k][j];
                }
            }
        }
        return result;
    }
    /*
     * imply matrices are square
     */
    unique_ptr<matrix> multiply_block(const matrix& m, int bs) {
        assert(h == w && m.w == m.h && w == m.w);
        unique_ptr<matrix> result(new matrix(h, m.w));
        int count = h / bs;
        for (int i = 0; i < count; i++)
            for (int j = 0; j < count; j++)
                for (int k = 0; k < count; k++)
                    result->int_multiply_block(*this,m,bs,i,j,k);
        return result;
    }
};
